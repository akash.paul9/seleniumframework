package test;

import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Test1_GoogleSearch_TestNG {
	
	WebDriver driver = null;
	
	@BeforeTest
    public void setUpTest() {
    	
        System.setProperty("webdriver.gecko.driver","/usr/local/share/gecko_driver/geckodriver");
		
		driver = new FirefoxDriver();
    	
    }
	
	@Test
	public void googlesearch() {
        
		driver.get("https://google.com/");
		driver.findElement(By.name("q")).sendKeys("Automation step by step");
		//driver.findElement(By.name("btnK")).click();
		driver.findElement(By.name("btnK")).submit();
		
	}
	
	@AfterTest
	public void tearDownTest() {
		driver.close();
		driver.quit();
		System.out.println("Test Completed");
		
	}

}
