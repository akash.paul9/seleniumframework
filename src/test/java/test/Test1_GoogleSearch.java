package test;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Test1_GoogleSearch {
	
	public static void main(String[] args) {
		googlesearch();
	}
	
	public static void googlesearch() {
        System.setProperty("webdriver.gecko.driver","/usr/local/share/gecko_driver/geckodriver");
		
		WebDriver driver = new FirefoxDriver();
		driver.get("https://google.com/");
		driver.findElement(By.name("q")).sendKeys("Automation step by step");
		//driver.findElement(By.name("btnK")).click();
		driver.findElement(By.name("btnK")).submit();
		//driver.close();
		System.out.println("Test Completed");
	}

}
