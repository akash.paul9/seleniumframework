package test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReportwithTestNG {
	
	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;
	WebDriver driver;
	
	@BeforeSuite
	public void setup() {
		
		htmlReporter = new ExtentHtmlReporter("extentTestNG.html");
		extent = new ExtentReports();
        extent.attachReporter(htmlReporter);

	}
	@BeforeTest
	public void setupTest() {
		
		System.setProperty("webdriver.gecko.driver","/usr/local/share/gecko_driver/geckodriver");
        driver = new FirefoxDriver();
	}
	
	@Test
	public void test() throws IOException {
		
		ExtentTest test = extent.createTest("Google Test one","This is a google test");
		driver.get("https://google.com/");
		test.pass("Entaring Google");
		test.log(Status.INFO, "This step shows usage of log(status, details)");
        test.info("This step shows usage of info(details)");
        test.pass("details", MediaEntityBuilder.createScreenCaptureFromPath("screenshot.png").build());
        test.addScreenCaptureFromPath("screenshot.png");
	}
	
	@AfterTest
	public void tearDownTest() {
		
		System.out.println("Test Completed");
		
	}
	
	@AfterSuite
	public void teardown() {
		extent.flush();
	}

}
