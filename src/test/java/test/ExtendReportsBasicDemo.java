package test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtendReportsBasicDemo {
	
	public static void main(String[] args) {
		
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("extent.html");
		ExtentReports extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
        
        ExtentTest test1 = extent.createTest("Google Test one","This is a google test");
		
        System.setProperty("webdriver.gecko.driver","/usr/local/share/gecko_driver/geckodriver");
        WebDriver driver = new FirefoxDriver();
        
        test1.log(Status.INFO, "Starting Test Case");
        driver.get("https://google.com/");
        
        test1.pass("Entaring Google");
        
        driver.findElement(By.name("q")).sendKeys("Automation step by step");
        test1.pass("Entering Text in searchbox");
        
        driver.findElement(By.name("btnK")).submit();
        test1.pass("Entering Text in searchbox");
        
        
        test1.pass("Closed the browser");
        test1.info("Test Completed");
        
        
ExtentTest test2 = extent.createTest("Google Test two","This is a google test");
		
        System.setProperty("webdriver.gecko.driver","/usr/local/share/gecko_driver/geckodriver");
        WebDriver driver1 = new FirefoxDriver();
        
        test2.log(Status.INFO, "Starting Test Case");
        driver1.get("https://google.com/");
        
        test2.pass("Entaring Google");
        
        driver1.findElement(By.name("q")).sendKeys("Automation step by step");
        test2.pass("Entering Text in searchbox");
        
        driver1.findElement(By.name("btnK")).submit();
        test2.fail("Entering Text in searchbox");
        
        
        test2.pass("Closed the browser");
        test2.info("Test Completed");
        extent.flush();

	}

}
